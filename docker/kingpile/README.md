# KingOfThePile #

## Objective: Create the largest pile of 'fresh' nuggets.
- Get each nugget name (filename) from http://<ipaddress>/pileking/
- Each nugget must contain (inside the file) a UNIQUE sentence or complete phrase of your choice about being king of the pile.
- Your nugget MUST be fresh, to gain points.
- To be fresh, your nugget must be formed and place on your pile (in your 'pile' directory) in a timely manner.
- Stale nuggets on your pile will affect your score negatively.

## Rules
This is the club's (aka your) server, treat it respectfully.  On that note here are some rules to keep the competition alive, and keep the play area reasonably open.

- DOSing system resources (cpu, disk, ram, etc) intentionally or accidently is not allowed
- Over using your share of system resources is not allowed.
- mshstech account may ONLY be used for two things...
 - creating/configuring YOUR account
 - troubleshooting/fixing YOUR account.
 (mshstech account may not be used for ANYTHING else. any other uses will result in diqualification)
- You may NOT alter the mshstech account or any of it's processes
- You may NOT alter any competition processes, reports, scoring, etc. This stops the competition, and NO, you Don't win.
- Other player's nugget piles are off limits (...no you can't do that)
- Players with zero score or less than half your score may NOT be DOSed  until their relative score increases.
- You may NOT delete or corrupt other player's accounts (traditional administrative actions may be OK; if the  previous rule is observed)

## Setup steps
- Copy `systrk` directory to server
- Create `pileking` folder: `mkdir /var/www/html/pileking`
- Create `js` folder: `mkdir /var/www/html/js`
- Download jquery, jqueryhighcharts, jqueryhighchartTable, and install in js folder
- Execute `syspile.bash` script as root, with ’NEW’ arg:  `sudo <path>/systrk/syspile.bash NEW`
- Press `<enter>` to stop the scoring engine
- Comment or remove line #67 from `<path>/systrk/syspile.bash`: `cat $scriptdir/syscntsrc.bash | openssl...`
- Delete file `<path>/systrk/syscntsrc.bash`

## Start the competion/scoring
Execute syspile.bash script as root, with ’NEW’ arg
```bash
sudo <path>/systrk/syspile.bash NEW
```