#!/bin/bash

## syscnt.bash ##
#
# Counting component of King of the Pile 
#
# count the number of fresh nuggets in each player's pile

#sesnkey="hello"
#nugget_log="var/.log"

if [ "$sesnkey" = "" ]; then echo "how can i be of service?"; exit; fi

scriptdir=$(dirname $(readlink -f "$0"))
tmpfl1=$scriptdir/var/.tempcntfile1
tmpfl2=$scriptdir/var/.tempcntfile2
stats=$scriptdir/var/.stats
let sleeptime=10
let starttime=$(date "+%s")

sleep 1
while :
do
    #unclear why this throws an error @ startup
    echo "" |  openssl enc -des3 -a -k $sesnkey -pbkdf2 > $tmpfl1

    touch $tmpfl2

    for player in $(ls /home | grep -v "mshstech" | grep -v "guest"); do
        let score=0
        let freshNuggets=0
        let staleNuggets=0
        lastNugget=""

        if [[ -s $nugget_log && -d /home/$player/pile ]]; then

            lastNugget=$starttime
            while read line
            do
                time=$(echo $line | cut -f1 -d:)
                stamp=$(echo $line | cut -f2 -d:)

                if [ -f /home/$player/pile/$stamp ]; then

                    let stamptime=$(date -d "$(echo $time | sed -r 's/(.{6})(..)(..)(..)/\1 \2:\3:\4/')" "+%s")
                    let filetime=$(stat -c %Y /home/$player/pile/$stamp)
                    if [ $(( $filetime - $stamptime )) -lt 120 ]; then
                        let size=$(stat -c "%s" /home/$player/pile/$stamp)
                        if [ $size -gt 4 ]; then
                            let freshNuggets++
                            lastNugget=$(stat -c %Y /home/$player/pile/$stamp)
#echo $player scores on...  time: $time   number: $stamp  total: $freshNuggets
                        fi
                    fi
                fi

            done <$nugget_log
            let staleNuggets=$(ls -1 /home/$player/pile | wc -l)-$freshNuggets
            let score=(4*$freshNuggets)-$staleNuggets

            let lastNugget=($(date "+%s")-$lastNugget)/60

        fi
#echo $player score....
#echo $score:$player:$freshNuggets:$staleNuggets:$lastNugget 

        cat $tmpfl1 |  openssl enc -des3 -d -a -k $sesnkey -pbkdf2 > $tmpfl2
        echo $score:$player:$freshNuggets:$staleNuggets:$lastNugget >> $tmpfl2
        cat $tmpfl2 |  openssl enc -des3 -a -k $sesnkey -pbkdf2 > $tmpfl1

    done

    cd $scriptdir
    mv $tmpfl1 $stats
    rm $tmpfl2

    sleep $sleeptime
done
