#!/bin/bash

## KingOfThePile ##
# Objective:create the largest pile of 'fresh' nuggets.
#       Get each nugget name (filename) from http://<ipaddress>/pileking/
#       Each nugget must contain (inside the file) a UNIQUE sentence or complete phrase of your choice about being king of the pile.
#       Your nugget MUST be fresh, to gain points.
#       To be fresh, your nugget must be formed and place on your pile (in your 'pile' directory) in a timely manner.
#       Stale nuggets on your pile will affect your score negatively.
#
# Rules
#  This is the club's (aka your) server, treat it respectfully.  On that note here are some rules to keep the competition alive, and keep the play area reasonably open.
# - DOSing system resources (cpu, disk, ram, etc) intentionally or accidently is not allowed
# - Over using your share of system resources is not allowed.
# - You may NOT alter the mshstech account or any of it's processes
# - You may NOT alter any competition processes, reports, scoring, etc. This stops the competition, and NO, you Don't win.
# - Other player's nugget piles are off limits (...no you can't do that)
# - Players with zero score or less than half your score may NOT be DOSed  until their relative score increases.
# - You may NOT delete or corrupt other player's accounts (traditional administrative actions may be OK; if the  previous rule is observed)
# - mshstech account may ONLY be used for two things...
#       - creating YOUR account
#       - troubleshooting/fixing YOUR account.
#      (mshstech account may not be used for ANYTHING else. any other uses will result in diqualification)
#
#


scriptdir=$(dirname $(readlink -f "$0"))
surp=$(echo $scriptdir | md5sum | cut -f1 -d' ')
export nugget_log="$scriptdir/var/.log"
export sesnkey=$(echo $RANDOM | md5sum | cut -f1 -d' ')

echo "Starting King of the Pile"

#prepare envionment
if [ -s $nugget_log ]
then
    if [[ "$1" = "NEW" || "$1" = "new" ]]
    then
        rm $nugget_log
    elif [[ "$1" = "" ]]
    then
        echo " Stale log file detected"
        echo "   To start a new round try..."
        echo "      $(basename $(readlink -f "$0")) NEW"
        echo "   To restart the previous round try..."
        echo "      $(basename $(readlink -f "$0")) <RestartCode>"
        echo " ...exiting."
        exit
    else
        if [[ "$1" != "$(md5sum $nugget_log | cut -f1 -d' ')" ]]
        then
            echo " Restart Code does not match the Stale Log.  The log may have been altered."
            echo "  To start a new round try..."
            echo "     $(basename $(readlink -f "$0")) NEW"
            echo " ...exiting."
            exit
        fi
    fi
fi

#echo strtng trk
$scriptdir/systrk.bash &
trkPID=$!

# delete this line once syscntsrc.bash is finalized
#cat $scriptdir/syscntsrc.bash | openssl enc -des3 -a -k $surp -pbkdf2 > $scriptdir/syscnt.enc

#echo strtng cnt
cat $scriptdir/syscnt.enc | openssl enc -des3 -d -a -k $surp -pbkdf2 > $scriptdir/syscnt.bash
chmod 774 $scriptdir/syscnt.bash
$scriptdir/syscnt.bash &
cntPID=$!

#echo strtng cln
$scriptdir/syscln.bash &

sleep 1
#echo strtng disp
$scriptdir/sysdisp.bash &
dispPID=$!

read -p $'press <enter> to exit\n\n' userInput

kill $dispPID
kill $cntPID
kill $trkPID

echo "RestartCode: $(md5sum $nugget_log | cut -f1 -d' ')"

exit