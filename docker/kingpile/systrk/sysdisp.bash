#!/bin/bash

## sysdisp.bash ##
#
# Display component of King of the Pile 
# 
# update the web page with most recent nugget and scores

if [ "$sesnkey" = "" ]; then echo "how can i be of service?"; exit; fi

scriptdir=$(dirname $(readlink -f "$0"))
mod="$scriptdir/var/mod.txt"
tmpfl1=$scriptdir/var/tempdspfile1
tmpfl2=$scriptdir/var/tempdspfile2
webfilename="index.html"
webfilename2="nugget.html"
webpath="/var/www/pileking"
stamp_display="$scriptdir/var/.new_nugget"
score_sheet=$scriptdir/var/.stats
let sleeptime=7

sleep 1
while :
do
    cat $score_sheet |  openssl enc -des3 -d -a -k $sesnkey -pbkdf2 | sort -gr > $tmpfl1
    sed -r 's/^(.*):(.*):(.*):(.*):(.*)$/<tr><td>\2<\/td><td>\1<\/td><td>\3<\/td><td>\4<\/td><td>\5<\/td><\/tr>/' $tmpfl1 > $tmpfl2

    stamp=$(cat $stamp_display)

    sed "/<tbody>/r $tmpfl2" $scriptdir/web/index.html.template |
     sed "s/NNUUGGEETT/$stamp/" |
      sed "s/MMOODD/$(cat $mod)/" > $scriptdir/web/index.html

    sed "s/NNUUGGEETT/$stamp/" $scriptdir/web/$webfilename2.template > $scriptdir/web/$webfilename2

    mv $scriptdir/web/$webfilename2 $webpath/$webfilename2
    mv $scriptdir/web/index.html $webpath/$webfilename

    rm $tmpfl1 $tmpfl2
    sleep $sleeptime
done