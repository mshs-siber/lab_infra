## systrk.bash ##
#
# Manage nugget dispensing
#

if [ "$sesnkey" = "" ]; then echo "how can i be of service?"; exit; fi

scriptdir=$(dirname $(readlink -f "$0"))
mod="$scriptdir/var/mod.txt"
#stamp_display="$scriptdir/stamp_display.txt"
nugget="$scriptdir/var/.new_nugget"
stamp="xxx"

sleep 1
touch $nugget_log
hash="$(md5sum $nugget_log | cut -f1 -d' ')"
let sleeptime=30
cp /dev/null $mod
cp /dev/null $nugget

while :
do
#echo starting trk loop
    if [ "$(md5sum $nugget_log | cut -f1 -d' ')" = "$hash" ]
    then
        stamp=$RANDOM
        echo $(date "+%y%m%d%H%M%S"):$stamp >> $nugget_log
        hash="$(md5sum $nugget_log | cut -f1 -d' ')"
        echo $stamp > $nugget
        echo "" > $mod
    else
        echo 'Ouch\! I have been hacked\!' > $mod 
    fi

    let sleeptime=$(( ( RANDOM % 25 ) + 5 ))
echo next nugget in: $sleeptime
    sleep $sleeptime

done
